import javafx.util.Pair;
import node.Component;
import node.remote.CommunicationChannel;

import java.io.File;
import java.io.FileNotFoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

/**
 * @author Dan Graur 11/18/2017
 */
public class Initiator {

    /**
     * args[0] - Registry IP
     * args[1] - Registry Port
     * args[2] - PID (since Java 1.8 does not allow this directly);
     * args[3] - path to file containing the peer configurations
     * args[4] - line index in the peer file (starts at 0)
     *
     * @param args contains the args of the Component
     */
    public static void main(String[] args) throws FileNotFoundException, RemoteException, InterruptedException {
        if (args.length != 5) {
            System.err.println("Usage: java Initiator <Reg-IP> <Reg-Port> <My-PID> <path-to-peer-config> <line-index>");

            return;
        }

        /* Configure the security manager */
        System.setProperty("java.security.policy", "file:generic.policy");
        System.setSecurityManager(new SecurityManager());

        /* Config the Component */
        Component theComponent = new Component(Integer.parseInt(args[2]));

        Pair<String, Map<String, CommunicationChannel>> result = configPeers(
                                args[0],
                                Integer.parseInt(args[1]),
                                theComponent,
                                args[3],
                                Integer.parseInt(args[4])
        );

        /* Get the name */
        theComponent.setName(result.getKey());

        /* Get the peers */
        theComponent.setPeers(result.getValue());

        System.out.println("Have managed to successfully create the Component");

        /* Start the Component */
        Thread ComponentThread = new Thread(theComponent);
        ComponentThread.start();

        /* Wait until the Component is finished, otherwise the GC might cause problems */
        ComponentThread.join();
    }

    /**
     * Publish the Component's comm channel to the registry, and collect the other channels as they become available
     *
     * @param registryIP the IP of the registry
     * @param registryPort the port of the registry
     * @param component the Component which is currently being instantiated
     * @param path the path to the peer configuration file
     * @param lineIndex the index in the configuration file corresponding to the Component being created
     * @return a pair containing the name of the new Component and a map with all of the created Components
     * @throws FileNotFoundException is thrown when the config file could not be found
     * @throws RemoteException is thrown when the registry could not be found
     */
    private static Pair<String, Map<String, CommunicationChannel>> configPeers(String registryIP,
                                                                               int registryPort,
                                                                               Component component,
                                                                               String path,
                                                                               int lineIndex) throws FileNotFoundException, RemoteException {
        /* Some initializations */
        Scanner fileReader = new Scanner(new File(path));
        Map<String, CommunicationChannel> peers = new HashMap<>();
        List<String> tempNameList = new ArrayList<>();

        String nameOfComponent = "";

        /* Get the registry */
        Registry registry = LocateRegistry.getRegistry(registryIP, registryPort);

        /* Read while there is a next line */
        for (int i = 0; fileReader.hasNextLine(); ++i) {
            /* Get the tokens, separated by ';'. Awaited structure: <name>;port */
            String[] tokens = fileReader.nextLine().split(";");
            String nameInRegistry =  tokens[0].trim();

            tempNameList.add(nameInRegistry);

            /* Add the current Component (as a stub) to the registry */
            if (i == lineIndex) {
                int port = Integer.parseInt(tokens[1].trim());

                CommunicationChannel stub = (CommunicationChannel) UnicastRemoteObject.exportObject(component, port);
                registry.rebind(nameInRegistry, stub);

                nameOfComponent = nameInRegistry;

                /* Check if this is a candidate node */
                if (tokens[2].trim().equals("c"))
                    component.setCandidate(true);
            }
        }

        for (String s : tempNameList) {
            System.out.println("Name: " + s);
        }

        /* Build up the peer list */
        for (String regName : tempNameList) {
            boolean collected = false;

            while (!collected) {
                try {
                    CommunicationChannel extractedStub = (CommunicationChannel) registry.lookup(regName);

                    peers.put(regName, extractedStub);

                    collected = true;

                } catch (NotBoundException e) {
                    //System.err.println("Could not collect the stub under the name: " + regName + ". Will go to sleep for a bit.");

                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }


        }

        /* Return the collected peers */
        return new Pair<>(nameOfComponent, peers);
    }
}
