package node;

import javafx.util.Pair;
import node.message.Message;
import node.message.MessageType;
import node.remote.CommunicationChannel;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * This is the node of the system
 *
 * @author Dan Graur 11/25/2017
 */
public class Component implements CommunicationChannel, Runnable, Serializable {
    /**
     * The pid of this component
     */
    private long pid;

    /**
     * The node's state object
     */
    private State state;

    /**
     * The name under which the component's stub is published
     */
    private String name;

    /**
     * Whether this node is trying to be elected.
     */
    private boolean candidate;

    /**
     * Whether this node is killed and will never be elected.
     */
    private boolean killed;

    /**
     * Whether this node has been elected.
     */
    private boolean elected;

    /**
     * Map which hold a queue for the outgoing links
     */
    private Map<String, CommunicationChannel> outgoingLinks;

    /**
     * This queue is used as an initial buffer at the process' input, in order to avoid heavy computation in the sendMessage method
     */
    private final Queue<Message> separatingQueue = new LinkedBlockingQueue<>();

    /**
     * The node's clock; used for assigning the id of the outgoing messages
     */
    private long sClock;

    /**
     * Used to log information.
     */
    private PrintWriter log;

    /**
     * Will hold the communication channels to the other peers
     */
    private Map<String, CommunicationChannel> peers;

    /**
     * The potential owner of the node
     */
    private String potentialOwner;

    /**
     * The current owner of the node
     */
    private String owner;


    /**
     * Constructor
     *
     * @param pid the symbolic pid of this process
     */
    public Component(long pid) {
        /* Set the PID */
        this.pid = pid;

        // In lab 3 links are bidirectional so we can keep using this.
        this.outgoingLinks = new HashMap<>();

        state = new State(0, -1, name);
        candidate = false;
        killed = false;
        elected = false;

        potentialOwner = null;
        owner = null;

        /* Logging code */
        try{
            String path = new File("").getAbsolutePath();
            if(path.contains("target\\classes")) path = path.replace("target\\classes", "");
            log = new PrintWriter(path + "/logs/log_component_" + pid + ".txt", "UTF-8");
            log.println(new Date().toString() + " Initialized Component with process id " + pid);
            log.flush();
        } catch (IOException e){
            e.printStackTrace();
        }

    }

    public void setPeers(Map<String, CommunicationChannel> peers) {
        /* Set the peers */
        this.peers = peers;

        /* Logging code */
        log.print(new Date().toString() + " Set new peers with process ids: ");

        for(String peerID : peers.keySet()){
            log.print(peerID + " ");
        }
        log.println();
        log.flush();
    }

    public long getPid() {
        return pid;
    }

    public String getName() {
        return name;
    }

    public long getsClock() {
        return sClock;
    }

    /**
     * This method will be used through RMI, hence the confusing name. Other
     * Processes will make use of it in order to send messages to this process
     *
     * @param message the received message
     * @throws RemoteException
     */
    @Override
    public synchronized void sendMessage(Message message) throws RemoteException {
        /* Add the message to the buffer queue */
        separatingQueue.add(message);
    }

    /**
     * Gets a previously received message (FIFO order)
     *
     * @return return a previously received message (FIFO order)
     */
    private Message getMessage() {
        Message returnedMessage = null;

        try {
            returnedMessage = ((LinkedBlockingQueue<Message>) separatingQueue).take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return returnedMessage;
    }

    /**
     * Send a message trough all known links
     *
     * @param message the message being sent
     * @throws RemoteException
     */
    private void sendMessageToLinks(Message message) throws RemoteException {
        for(Iterator<Map.Entry<String, CommunicationChannel>> entryIterator = outgoingLinks.entrySet().iterator();
            entryIterator.hasNext();){
            Map.Entry<String, CommunicationChannel> pair = entryIterator.next();

            int index = Integer.parseInt(pair.getKey());

            pair.getValue().sendMessage(message);
        }
    }

    /**
     * The main loop for the oridnary process
     *
     * @throws RemoteException thrown when there is an RMI error
     */
    private void ordinaryLoop() throws RemoteException {
        while(true) {
            Message message = getMessage();
            State receivedState = message.getState();

            System.out.println("I have received a message: " + message.toString());

            /* Better owner available */
            if (receivedState.compareTo(state) > 0) {
                System.out.println("I'm in the first clause");

                /* Get the name of the owner */
                potentialOwner = message.getProcName();

                /* Make the changes to the local state */
                state.setOwnerId(receivedState.getOwnerId());
                state.setLevel(receivedState.getLevel());

                /* If there was no previous owner, immediately assign */
                if (owner == null)
                    owner = potentialOwner;

                /* Kill (or ack capture if immediate assignment of new (since there will not be a kill ack requested before this)) owner */
                peers.get(owner).sendMessage(
                        new Message(
                                pid,
                                name,
                                sClock,
                                MessageType.REGULAR,
                                receivedState,
                                ""
                    )
                );

                System.out.println("I did get here");
            }
            /* Former owner has ack'd its own termination */
            else if (receivedState.compareTo(state) == 0 && message.getProcName().equals(owner)) {
                System.out.println("I'm in the second clause");

                /* Assign new owner */
                owner = potentialOwner;

                /* Ack capture to new owner */
                peers.get(owner).sendMessage(
                        new Message(
                                pid,
                                name,
                                sClock,
                                MessageType.REGULAR,
                                receivedState,
                                ""
                        )
                );

                System.out.println("I did get here");
            }

        }
    }


    /**
     * The main loop for the candidate process
     *
     * @throws RemoteException thrown when there is an RMI error
     */
    private void candidateLoop() throws RemoteException{
        /* Set state owner to pid */
        state.setOwnerId(pid);

        Random rand = new Random();
        outgoingLinks = getMyOutgoingLinks();

        /* Main candidate loop */
        while(!outgoingLinks.isEmpty()) {


            /* Add the !killed condition here to facilitate the goto */
            if (!killed) {
                String key = (String) outgoingLinks.keySet().toArray()[rand.nextInt(outgoingLinks.size())];
                System.out.println("Sending a message to " + key);

                /* Get random link a request to */
                CommunicationChannel link = outgoingLinks.get(key);

                /* Send the message */
                link.sendMessage(
                        new Message(
                                pid,
                                name,
                                sClock,
                                MessageType.REGULAR,
                                state,
                                ""
                        )
                );

                System.out.println("Message sent to " + key);
            }

            Message receivedMessage;

            /* Ignore all messages of lower level (and lower id) - these are worse offers */
            do {
                System.out.println("Awaiting message");

                receivedMessage = getMessage();

                System.out.println("I have received a message: " + receivedMessage);
            } while(receivedMessage.getState().compareTo(state) < 0);

            /* Received a capture ack from a node */
            if (receivedMessage.getState().getOwnerId() == pid && !killed) {
                /* Increment level, and remove link from future visits */
                state.incrementLevel();
                outgoingLinks.remove(receivedMessage.getProcName());

                System.out.println("Have captured: " + receivedMessage.getProcName());
            } else {
                /* Acknowledge kill */
                peers.get(receivedMessage.getProcName()).sendMessage(
                        new Message(
                                pid,
                                name,
                                sClock,
                                MessageType.REGULAR,
                                receivedMessage.getState(),
                                ""
                        )
                );

                System.out.println("I have been killed by: " + receivedMessage.getProcName());

                /* Mark node as killed */
                killed = true;

                /* Implicitly go and receive messages */
            }
        }

        if (!killed) {
            elected = true;
            System.out.println("I HAVE BEEN ELECTED: " + pid);
        }
    }

    @Override
    public void run() {

        System.out.println("I'm in the run method " + name + "\nI am a candidate? " + candidate);

       if (candidate)
           try {
               candidateLoop();
           } catch (RemoteException e) {
               e.printStackTrace();
           }
       else
           try {
               ordinaryLoop();
           } catch (RemoteException e) {
               e.printStackTrace();
           }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCandidate(boolean candidate) {
        this.candidate = candidate;
    }

    private Map<String,CommunicationChannel> getMyOutgoingLinks() {
        Map<String, CommunicationChannel> myOutgoingLinks = new HashMap<>(peers);

        myOutgoingLinks.remove(name);

        return myOutgoingLinks;
    }
}
