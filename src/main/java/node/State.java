package node;

import java.io.Serializable;

/**
 * A pair consisting of a level and owner-id.
 */
public class State implements Comparable, Serializable {

    private int level;
    private long ownerId;
    private String name;


    public State(int level, long ownerId, String name) {
        this.level = level;
        this.ownerId = ownerId;
        this.name = name;
    }

    public void incrementLevel() {
        ++level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel(){
        return level;
    }

    public long getOwnerId(){
        return ownerId;
    }

    public void setLevel(int level){
        this.level = level;
    }

    public void setOwnerId(long ownerId){
        this.ownerId = ownerId;
    }

    public void setState(State otherState){
        this.level = otherState.getLevel();
        this.ownerId = otherState.getOwnerId();
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof State))
            return -1;

        State that = (State) o;

        int diff = this.level - that.getLevel();

        /* If equal level --> use ownerID as a tie breaker (can also be zero) */
        if (diff == 0)
            return (int) (this.ownerId - that.getOwnerId());

        /* Else use only the level difference */
        return diff;
    }


    @Override
    public String toString() {
        return "State{" +
                "level=" + level +
                ", ownerId=" + ownerId +
                '}';
    }
}
