package server;

import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author Dan Graur 11/25/2017
 */
public class RegistryServer {

    public static void main(String[] args) throws IOException {

        System.setProperty("java.security.policy", "file:./src/main/resources/generic.policy");
//        System.setSecurityManager(new SecurityManager());

        Registry registry = LocateRegistry.createRegistry(1099);

        System.in.read();
    }
}
